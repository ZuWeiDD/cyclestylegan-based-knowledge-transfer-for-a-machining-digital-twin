# CycleStyleGAN-based knowledge transfer for a machining digital twin

A repository for the open access files related to the publication "CycleStyleGAN-based knowledge transfer for a machining digital twin" by Evgeny Zotov and Visakan Kadirkamanathan from the University of Sheffield.